﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public enum VolumeLevel
{
    Off,
    Low,
    Medium,
    High
}

[Serializable]
public class MuffledAudioClipSet
{
    public AudioClip normalClip;
    public AudioClip muffledClip;
}

public class AudioManager : Singleton<AudioManager>
{
    public static event Action<int> VolumeLevelChanged;
#pragma warning disable CS0414, CS0649 // Never used & never assigned warning (unity)

    [Header("Volume Control")]
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField, ReadOnly] private VolumeLevel _currentVolumeLevel;

    [Header("BGM")]
    [SerializeField] private AudioSource _bGMSource;
    [SerializeField] private List<AudioClip> _musicTracks;
    [SerializeField] private List<MuffledAudioClipSet> _muffledBGMSets;
    [SerializeField] private bool _playOnAwake = false;
    [SerializeField] private bool _loop = true;
    [SerializeField] private float _bGMVolume = .7f;
    [SerializeField, ReadOnly] private AudioClip _currentBGM;
    [SerializeField, ReadOnly] private MuffledAudioClipSet _currentMuffledBGMSet;

    [Header("SFX")]
    [SerializeField] private AudioSource _sFXSource;
    [SerializeField] private List<AudioClip> _soundEffects;
    [Space]
    [SerializeField] private float _lowPitchRange = 0.75f;
    [SerializeField] private float _highPitchRange = 1.25f;
    [Space]
    public string menuItemSFXPrefix = "Select";


    [Header("Other")]
    public List<Sprite> volumeSprites;
#pragma warning restore CS0414, CS0649 // Never used & never assigned warning (unity)

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(Instance);

        if (_bGMSource == null)
        {
            var go = new GameObject("BackgroundMusic");
            go.transform.SetParent(transform);
            _bGMSource = go.AddComponent<AudioSource>();
        }
        if (_musicTracks.Any())
        {
            _currentBGM = _bGMSource.clip = _musicTracks[0];
        }
        _bGMSource.loop = _loop;
        _bGMSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Master")[0];
        _bGMSource.volume = _bGMVolume;

        if (_sFXSource == null)
        {
            var go = new GameObject("SoundEffects");
            go.transform.SetParent(transform);
            _sFXSource = go.AddComponent<AudioSource>();
        }

        _sFXSource.outputAudioMixerGroup = _audioMixer.FindMatchingGroups("Master")[0];
    }

    // Start is called before the first frame update
    public void Init(int volumeLevel)
    {
        SetVolumeLevel(volumeLevel);

        _bGMSource.playOnAwake = _playOnAwake;
        if (_playOnAwake && !_bGMSource.isPlaying && _currentVolumeLevel != VolumeLevel.Off) _bGMSource.Play();
    }

    /// <summary>
    /// Loads and plays a sound using a sound prefix
    /// Perfect for playing a variety of sound effects
    /// </summary>
    /// <param name="soundNamePart"></param>
    public void PlaySound(string soundNamePart, bool pitchRandomizer = true)
    {
        if (_currentVolumeLevel == VolumeLevel.Off) return;
        try
        {
            var soundFiles = _soundEffects.Where(w => w.name.IndexOf(soundNamePart, StringComparison.OrdinalIgnoreCase) != -1).ToList();
            var randomSound = soundFiles.ElementAt(Random.Range(0, soundFiles.Count()));

            if (pitchRandomizer) _sFXSource.pitch = Random.Range(.85f, 1.10f);
            else _sFXSource.pitch = 1;

            if (randomSound.LoadAudioData())
            {
                if (randomSound.loadState == AudioDataLoadState.Loaded)
                    _sFXSource.PlayOneShot(randomSound);
            }
        }
        catch (Exception e)
        {
            Debug.Log("Play Sound: " + soundNamePart + " [" + e.Message + "]");
        }
    }

    public void MuffleBackgroundMusic(bool muffleAudio = true)
    {
        if (!_muffledBGMSets.Any() || _currentVolumeLevel == VolumeLevel.Off) return;

        var timeInTrack = _bGMSource.time;

        if (muffleAudio)
        {
            var clipSet = _muffledBGMSets.FirstOrDefault(x => x.normalClip == _currentBGM);
            if (clipSet != null)
            {
                _currentMuffledBGMSet = clipSet;
            }
            else return;

            _bGMSource.clip = _currentMuffledBGMSet.muffledClip;
        }
        else
        {
            if (_bGMSource.clip == _currentBGM || _currentMuffledBGMSet == null) return;
            _bGMSource.clip = _currentMuffledBGMSet.normalClip;
        }

        _bGMSource.Play();
        _bGMSource.time = timeInTrack;
    }

    

    public void SetVolumeLevel(int volumeLevel, bool broadcast = true)
    {
        _currentVolumeLevel = (VolumeLevel)Mathf.Clamp(volumeLevel, 0, 3);
        Debug.Log("Setting volume to " + _currentVolumeLevel);

        if (broadcast)
        {
            VolumeLevelChanged?.Invoke((int)_currentVolumeLevel);
        }
        switch (_currentVolumeLevel)
        {
            case VolumeLevel.Off:
                _audioMixer.SetFloat("Volume", Mathf.Log10(0) * 50);
                if (_bGMSource.isPlaying) _bGMSource.Stop();
                break;

            case VolumeLevel.Low:
                _audioMixer.SetFloat("Volume", Mathf.Log10(.25f) * 50);
                if (!_bGMSource.isPlaying) _bGMSource.Play();
                break;

            case VolumeLevel.Medium:
                _audioMixer.SetFloat("Volume", Mathf.Log10(.5f) * 50);
                if (!_bGMSource.isPlaying) _bGMSource.Play();
                break;

            case VolumeLevel.High:
                _audioMixer.SetFloat("Volume", Mathf.Log10(1) * 50);
                if (!_bGMSource.isPlaying) _bGMSource.Play();
                break;

            default:
                break;
        }

        PlaySound(menuItemSFXPrefix);
    }

    public Sprite GetVolumeSprite(int level = -1)
    {
        if (level < 0 || level >= Enum.GetValues(typeof(VolumeLevel)).Length)
            level = (int)_currentVolumeLevel;
        return volumeSprites[level];
    }
}
